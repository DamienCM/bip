import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread, imsave
from skimage.morphology import binary, dilation, disk, closing, black_tophat
from skimage.color import rgba2rgb, rgb2gray, gray2rgb, rgb2hsv
from skimage.filters import difference_of_gaussians, threshold_otsu
from skimage.measure import find_contours, regionprops, label
import time


IMAGE_PATH = 'ph2-dataset/IMD027.bmp'
BORDER_SIZE = 10
original = imread(IMAGE_PATH)
original = original[BORDER_SIZE:-BORDER_SIZE, BORDER_SIZE:-BORDER_SIZE, :]

gray = rgb2gray(original)

fig,axs = plt.subplots(1,1,figsize=(10,10))
bh = black_tophat(gray, selem=disk(4))
n_bh = (bh - np.mean(bh))/np.std(bh)
th_otsu = threshold_otsu(n_bh)
th_bh = np.where(n_bh < th_otsu, 0, 1)
d_th_bh = dilation(th_bh, selem=disk(2))
labels = label(d_th_bh, connectivity=1)
regions = regionprops(labels)
cleaned = d_th_bh.copy()
for region in regions:
    if region.area <= 50:
        cleaned[labels == region.label] = 0
axs.imshow(d_th_bh, cmap="gray")
axs.set_title("Binary(Otsu)")

plt.show()