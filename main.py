# imports the required packages
from numpy.lib.function_base import disp
from skimage.io import imread, imsave
import os
import math
import numpy as np
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu, threshold_local
import skimage
from skimage.color import rgb2gray
from skimage.io.manage_plugins import plugin_order
from skimage.measure import regionprops, label, regionprops_table
from skimage.morphology import closing
import pandas as pd


def evaluate_melanoma(image_path, B_LIMIT=0.1, C_LIMIT=100,  display=False, printing=False):
    """
    Fonction de base pour évaluer si une image est de type melanoma ou non. 
        Elle est très peu performante, env. 15% de similarité avec les résultats des dermatologues.

    Args:
        image_path ([string]): [chemin du fichier image a traiter]
        display (bool, optional): [choisir si on affiche les images intermediaires ou non]. Defaults to False.
        printing (bool, optional): [choisir si on affiche dans le stdo des messages de log]. Defaults to False.

    Returns:
        [tuple(solidity : flaot, global_std : float, predict_melamona : float)]: [critères C et B et prediction]
    """

    # Grab the first image
    original = imread("ph2-dataset/"+image_path)
    # Convert to grayscale then apply an Otsu thresholding
    grayscale = rgb2gray(original)
    # s = grayscale.shape[0]
    # s = s + 1 if not s%2 else s
    thresh_otsu_value = threshold_otsu(grayscale)
    thresholded_image = grayscale < thresh_otsu_value

    # display the thresholded if requested
    if display:
        plt.figure("original")
        plt.imshow(grayscale, cmap="gray")
        plt.figure("otsu")
        plt.imshow(thresholded_image, cmap="gray")

    # Computes the regions detected on the image
    label_img = label(thresholded_image)
    regions = regionprops(label_img)

    # extract the bounding box coordinates, areas, solidity
    areas = [prop.area for prop in regions]
    bboxs = [prop.bbox for prop in regions]
    solidities = [prop.solidity for prop in regions]

    areas_sort = np.sort(areas)
    index = areas.index(areas_sort[-1])

    bbox = bboxs[index]
    area = areas[index]
    solidity = solidities[index]

    # print the informations if requested
    if printing:
        print(
            f'Max region properties : \n-solidity : {solidity} \n-area = {area}')

    minr, minc, maxr, maxc = bbox
    bx = (minc, maxc, maxc, minc, minc)
    by = (minr, minr, maxr, maxr, minr)

    # display the bouding box of the region if requested
    if display:
        plt.figure("region")
        plt.imshow(thresholded_image, cmap="gray")
        plt.plot(bx, by, '-b', linewidth=2.5)
        plt.show()

    # computes the standard deviation of the image for each channel and sums up
    R_std = np.std(original[:, :, 0])
    G_std = np.std(original[:, :, 1])
    B_std = np.std(original[:, :, 2])

    global_std = R_std + G_std + B_std

    # predicts if the image is a melanoma or not
    predicted_melanoma = False

    # criteria for melanoma : C > 100 and B < 0.95
    if solidity < B_LIMIT and global_std > C_LIMIT:
        predicted_melanoma = True

    return solidity, global_std, predicted_melanoma


# Re-evaluate the melanoma on the test set
COMPUTE_DATASET = True


# Verify that the dateset is present
try:
    files_list = os.listdir("ph2-dataset")
    print("Dataset found")
    # print(len(files_list))
except Exception as e:
    print("Error loading the dataset: ", e)

if COMPUTE_DATASET:
    d = {}
    for image in files_list:
        d[image] = evaluate_melanoma(image)[2]

    df = pd.DataFrame(d.items(), columns=['Image', 'Type'])
    df.to_csv("output.csv")
else:
    try:
        df = pd.read_csv("output.csv")
    except Exception as e:
        print("Error loading the computed result: ", e)

ground_truth = pd.read_csv("PH2_dataset.csv")

POSITIVES = 40
NEGATIVES = 160

success_pos = 0
success_neg = 0

for i, element in enumerate(ground_truth["Melanoma"]):
    # if predcting the melanoma is correct
    # print(element, df["Type"][i])
    if (element == "X" and df["Type"][i]):
        success_pos += 1
    if  (element != "X" and not df["Type"][i]):
        success_neg += 1


print(f"true positives : {success_pos}; ratio = {success_pos/POSITIVES}")
print(f"true negatives : {success_neg} ; ratio = {success_neg/NEGATIVES}")
mean = 0.5 * (success_pos/POSITIVES + success_neg/NEGATIVES)
print(f"Mean : {mean}")
