# %%
import numpy as np
import matplotlib.pyplot as plt
from skimage.io import imread
from skimage.measure import label, regionprops
from skimage.color import rgb2gray, rgb2lab
from skimage.transform import rotate
import os
import pandas as pd
from tqdm import tqdm


def centree_reduite(X):
    X = (X-np.mean(X))/np.std(X)
    return X


def compute_AD_criteria(IMAGE_PATH):
    """[Compute A criterias and D criterias adapted from article 2]


    Args:
        IMAGE_PATH ([string]): [path to the image]

    Returns:
        [float]: [A criteria]
    """
    # --- Load image and extract basic informations ---
    original = imread(IMAGE_PATH)
    gray = rgb2gray(original)
    mask = np.where(gray > 0, 1, 0)

    # --- Identify the regions ---
    # Grabs the biggest region and extract its angle and center
    labels = label(mask)
    props = regionprops(labels)
    p = max(props, key=lambda x: x.area)
    y0, x0 = p.centroid
    orientation = p.orientation

    # --- Convert colorspace to L*a*b as in the article ---
    lab = rgb2lab(original)

    # --- Rotate the image to align the ellipse on x, y axis ---
    rot_lab = rotate(lab, -p.orientation*180/np.pi,
                     center=(x0, y0), resize=True)
    rot_original = rotate(original, -p.orientation*180 /
                          np.pi, center=(x0, y0), resize=True)
    rot_gray = rgb2gray(rot_original)
    rot_mask = np.where(rot_gray > 0, 1, 0)

    # --- Grab the new region and its properties ---
    labels = label(rot_mask)
    props = regionprops(labels)
    p = max(props, key=lambda x: x.area)
    total_area = p.area
    x0, y0 = p.centroid
    minor_axis = p.minor_axis_length
    major_axis = p.major_axis_length
    # --- Compute the D criteria ---
    D_approximate_diameter = 0.5 * (minor_axis + major_axis)

    # --- Divise the image in 4 corners ---
    # Instead of following the article defining 20x20 pixels zones, for ease reasons, we considered only one region per corner
    tl, br, tr, bl = rot_lab[:int(x0), :int(y0), :], rot_lab[int(x0):, :int(
        y0), :], rot_lab[:int(x0), int(y0):, :], rot_lab[int(x0):, int(y0):, :]
    corners = [tl, tr, br, bl]

    # --- Get each corner mean for each canal to conpute their euclidan distance ---
    means = []
    areas = []
    for i in range(4):
        Lchannel = corners[i][:, :, 0]
        achannel = corners[i][:, :, 1]
        bchannel = corners[i][:, :, 2]
        L = np.sum(Lchannel)/np.count_nonzero(Lchannel)
        a = np.sum(achannel)/np.count_nonzero(achannel)
        b = np.sum(bchannel)/np.count_nonzero(bchannel)

        # is colored matrice = 0 if not colored or 1 if colored
        is_colored = np.logical_or(np.logical_or(np.not_equal(
            Lchannel, 0), np.not_equal(achannel, 0)), np.not_equal(bchannel, 0))
        areas.append(np.sum(is_colored))
        means.append(np.array([L, a, b]))

    # --- Compute the euclidian distance between each corner to evaluate the asymmetry (for each color, luminance and area) ---
    def euclidian_distance(a, b):
        return np.sqrt(np.sum((a-b)**2))

    A_criteria_color = euclidian_distance(means[0], means[1]) + euclidian_distance(means[0], means[2]) + euclidian_distance(
        means[0], means[3]) + euclidian_distance(means[1], means[2]) + euclidian_distance(means[1], means[3]) + euclidian_distance(means[2], means[3])
    A_criteria_luminance = euclidian_distance(means[0][0], means[1][0]) + euclidian_distance(means[0][0], means[2][0]) + euclidian_distance(
        means[0][0], means[3][0]) + euclidian_distance(means[1][0], means[2][0]) + euclidian_distance(means[1][0], means[3][0]) + euclidian_distance(means[2][0], means[3][0])
    A_criteria_area = euclidian_distance(areas[0], areas[1]) + euclidian_distance(areas[0], areas[2]) + euclidian_distance(
        areas[0], areas[3]) + euclidian_distance(areas[1], areas[2]) + euclidian_distance(areas[1], areas[3]) + euclidian_distance(areas[2], areas[3])

    # Returns each criteria
    return A_criteria_color, A_criteria_luminance, A_criteria_area, D_approximate_diameter


# %%

# --- For each image compute its scores and store them---
# Empty lists to store the scores
A_scores_color = []
A_scores_luminance = []
A_scores_area = []
D_scores = []
files = os.listdir('./ph2-dataset/')

# Loop through all the images and computes their scores
for f in tqdm(os.listdir('ph2-processed/')):
    score_color, score_luminance, score_area, score_diameter = compute_AD_criteria(
        'ph2-processed/'+f)
    A_scores_color.append(score_color)
    A_scores_luminance.append(score_luminance)
    A_scores_area.append(score_area)
    D_scores.append(score_diameter)


# --- Standardized the scores ---
A_scores_color = np.array(A_scores_color)
A_scores_luminance = np.array(A_scores_luminance)
A_scores_area = np.array(A_scores_area)
D_scores = np.array(D_scores)
A_scores_color = centree_reduite(A_scores_color)
A_scores_luminance = centree_reduite(A_scores_luminance)
A_scores_area = centree_reduite(A_scores_area)
D_scores = centree_reduite(D_scores)


# %%

# --- Save the scores into a csv file ---
# load the dataset containing the ground truth
GROUND_TRUTH = pd.read_csv("PH2_dataset.csv")
# create a dataframe containing the scores to save
df = pd.DataFrame({
    'file': files,
    'positive': GROUND_TRUTH["Melanoma"].values == 'X',
    'A_color_score': A_scores_color,
    'A_luminance_score': A_scores_luminance,
    'A_area_score': A_scores_area,
    'D_score': D_scores,
})
# saving
df.to_csv('optimize.csv', index=False)
