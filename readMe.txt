Le zip contient:

-le notebook "main.ipynb"
-des images sauvegardées pour l'affichage dans le notebook
-des fichiers csv utilisés pour la simplification des calculs
-un dossier vide "ph2-processed" vide pour ne pas à faire le calcul à chaque fois. L'execution du notebook une seule fois permet de remplir ce dossier

Veuillez noter que certaines cellules prennent un peu de temps à s'exécuter (environ 5 à 10 mins). Veuillez s'il vous plait attendre l'exécution de toutes les cellules. 

En vous remerciant.