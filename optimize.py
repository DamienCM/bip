import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm


# --- Load the dataset and extract values ---
# Load
df = pd.read_csv('optimize.csv')
# Extract
A_criteria_color = df['A_color_score'].values
A_criteria_luminance = df['A_luminance_score'].values
A_criteria_area = df['A_area_score'].values
D_criteria = df['D_score'].values
criterias = [A_criteria_color, A_criteria_luminance,
             A_criteria_area, D_criteria]
positives = df['positive'].values


def accuracy(coeffs, criterias, reality):
    """Compute the accuracy for a set of coefficients.

    Args:
        coeffs ([list of floats]): [list of coefficients]
        criterias ([list of 1xN ndarray]): [lists des differents scores pour chaque image et chaque critere]
        reality ([ndarray 1xN bool]): [true if malanoma is cancerous false if not]

    Returns:
        [float]: [accuracy = 0.5(TP/P + TN/N)]
    """    
    P = 40  # Positives number
    N = 160  # Negatives number

    TP = np.zeros_like(reality)  # True positives
    TN = np.zeros_like(reality)  # True negatives

    # Compute the scores through the array of criteria evaluation of each image
    scores = coeffs[0]*criterias[0] + coeffs[1] * \
        criterias[1] + coeffs[2]*criterias[2] + coeffs[3]*criterias[3]
    # Compute the true positives and true negatives arrays
    TP = np.logical_and(scores > 1, reality)
    TN = np.logical_and(scores <= 1, np.logical_not(reality))
    # Converts the arrays to integers
    TP = np.sum(TP)
    TN = np.sum(TN)
    # Compute the accuracy and returns it
    accuracy = 0.5*(TP/P + TN/N)
    return accuracy


# %%
# --- Algorithm to optimize the coefficients applied to each criteria ---

N = 40  # Each axis length
N_coeffs = len(criterias)  # Number of coefficients == dimensions of the space
axis = np.linspace(-5, 5, N)

grid = np.meshgrid(*[axis for i in range(N_coeffs)])

acc = np.zeros([N for i in range(N_coeffs)])

# Compute the accuracy for each point on the grid i.e a unique set of coefficients 
print('Computing the accuracy for each point on the grid...')
print('This may take a while...')
print(f'Total of operations = {N**N_coeffs}')
for indexes in tqdm(np.ndindex(acc.shape)):
    coef = [ax.item(*indexes) for ax in grid]
    acc[indexes] = accuracy(coef, criterias, positives)


# %%
# --- Extract the best accuracy and its corresponding coefficients ---
index = np.unravel_index(np.argmax(acc, axis=None), acc.shape)
coeffs = [ax.item(*index) for ax in grid]
print(f'{coeffs=}')
print(f'{acc[index]=}')

# # %%
# # --- Plot the accuracy grid ---
# fig = plt.figure()
# ax = fig.add_subplot(projection='3d')
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')
# s = ax.scatter(*grid, c=acc, marker='x', cmap="jet")
# fig.colorbar(s)
