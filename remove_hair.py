# %%
from mahotas.thresholding import otsu
import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread, imsave
from skimage.morphology import binary, dilation, erosion, disk, square, closing, black_tophat
from skimage.color import rgba2rgb, rgb2gray, gray2rgb, rgb2hsv
from skimage.filters import difference_of_gaussians, threshold_otsu
from skimage.filters.rank import mean
from skimage.measure import find_contours, regionprops, label
from scipy.signal import convolve2d
import time
import mahotas
from skimage.morphology.selem import square
# %%


def identify_hairs(IMAGE_PATH, plotting=True, saving=True, close_fig = False, **kwargs):
    """
    This function identifies the hairs
    """
    # --- PROGRAM PARAMETERS ---
    options = {
        'BORDER_SIZE': 50,  # pixels to remove from the border
        'DOG_SIGMA': 2,  # sigma passed to the dog gaussian
        'THRESH_FACTOR': 0.9,  # threshold factor
        'REMOVAL_REGION_SIZE': 100,  # Size of small regions to remove
        'REMOVAL_CIRC_RATIO': 0.35,  # Circularity ratio of smalls regions to remove
        'DILATATION_SIZE': 3,  # Size of dilation to connect lines
        # Type of connectivity for regions to connect 0 (little), 1 (big)
        'REGION_CONNECTIVITY': 1,
        'HAIRS_LENGTH': 30,  # minimum length of a hairs in pixels
        'HAIRS_WIDTH_LENGTH_RATIO': 0.4,  # hairs width / length
        'HAIRS_MAX_SOLIDITY': 0.35,  # maximum solidity of a hairs crossing
        'CLOSING_SIZE': 10,  # Size of closing to fill gaps within hairs
        'EROSION_SIZE': 2,  # Size of erosion to make hairs thinner
    }
    options.update(kwargs)
    # Apply kwargs to options
    BORDER_SIZE = options['BORDER_SIZE']
    DOG_SIGMA = options['DOG_SIGMA']
    THRESH_FACTOR = options['THRESH_FACTOR']
    REMOVAL_REGION_SIZE = options['REMOVAL_REGION_SIZE']
    REMOVAL_CIRC_RATIO = options['REMOVAL_CIRC_RATIO']
    DILATATION_SIZE = options['DILATATION_SIZE']
    REGION_CONNECTIVITY = options['REGION_CONNECTIVITY']
    HAIRS_LENGTH = options['HAIRS_LENGTH']
    HAIRS_WIDTH_LENGTH_RATIO = options['HAIRS_WIDTH_LENGTH_RATIO']
    HAIRS_MAX_SOLIDITY = options['HAIRS_MAX_SOLIDITY']
    CLOSING_SIZE = options['CLOSING_SIZE']
    EROSION_SIZE = options['EROSION_SIZE']
    # --- END PROGRAM PARAMETERS ---

    # --- Figure parameters ---
    if plotting:
        fig, axs = plt.subplots(2, 3, figsize=(16, 9),)
        axs = axs.ravel()
        for ax in axs:
            ax.axis('off')
        fig.tight_layout()

    # Import and preprocess image
    original = imread(IMAGE_PATH)
    # Remove borders (y'a des bugs dans les coins)
    original = original[BORDER_SIZE:-BORDER_SIZE, BORDER_SIZE:-BORDER_SIZE, :]
    # Converts to HSL and keep luminance
    gray = rgb2gray(original)
    hsl_l = rgb2hsv(original)[:, :, 2]
    # Display original image
    if plotting:
        axs[0].set_title('Original')
        axs[0].imshow(original)
        axs[5].set_title('Detection')
        axs[5].imshow(original)

    # # --- Apply difference of gaussian ---
    # --- Apply difference of gaussian ---
    dog = difference_of_gaussians(hsl_l, low_sigma=DOG_SIGMA)
    dog = (dog - np.min(dog))
    dog = dog/np.max(dog)
    if plotting:
        axs[1].set_title("DoG")
        pcm = axs[1].imshow(dog)
    # fig.colorbar(pcm, ax=axs[1])

    # --- Binary mask creation ---
    THRESH = np.mean(dog)*THRESH_FACTOR

    binary_rc = dog.copy()
    binary_rc[binary_rc < THRESH] = 0
    binary_rc[binary_rc >= THRESH] = 1
    binary_rc = 1 - binary_rc
    if plotting:
        axs[2].imshow(1-binary_rc, cmap='gray')
        axs[2].set_title("Binary(0.9*mean)")

    # --- Undesired objects removal ---
    # 1. Too small objects
    labels = label(binary_rc, connectivity=1)
    regions = regionprops(labels)
    cleaned = binary_rc.copy()
    for region in regions:
        if region.area <= 2:
            cleaned[labels == region.label] = 0
    # 2. we remove small circular objects
    labels = label(cleaned)
    regions = regionprops(labels)
    for region in regions:
        if region.area < REMOVAL_REGION_SIZE and region.minor_axis_length/region.major_axis_length > REMOVAL_CIRC_RATIO:
            cleaned[labels == region.label] = 0
        # contours = find_contours(labels == region.label, 0.5)

    # Then display the image
    if plotting:
        axs[3].imshow(1-cleaned, cmap='gray')
        axs[3].set_title("Cleaned")

    # --- Dilation ---
    # We dilate to connect lines (without beeing annoyed by dots growing since we removed them)
    dilated = dilation(cleaned, disk(DILATATION_SIZE))
    if plotting:
        axs[4].imshow(1-dilated, cmap='gray')
        axs[4].set_title("Dilatation")

    # --- Find hairs ---
    filled = np.zeros_like(gray)
    detected = np.zeros_like(original)

    label_img = label(dilated, connectivity=REGION_CONNECTIVITY)
    regions = regionprops(label_img)

    for region in regions:
        # # Usefull code to display the contours
        # #
        # # longueur suffisante
        # if region.major_axis_length > HAIR_LENGTH:
        #     contours = find_contours(label_img == region.label, 0.5)
        #     for c in contours:
        #         axs[4].plot(c[:, 1], c[:, 0], 'yellow')
        #         axs[4].text(region.centroid[1], region.centroid[0],
        #                     region.label, color='orange')
        #         print(f'{region.solidity=}')
        # # Suffisament ellipsoide
        # if region.minor_axis_length/region.major_axis_length < HAIR_WIDTH_LENGTH_RATIO:
        #     contours = find_contours(label_img == region.label, 0.5)
        #     for c in contours:
        #         axs[4].plot(c[:, 1], c[:, 0], 'green')
        #         axs[4].text(region.centroid[1], region.centroid[0],
        #                     region.label, color='green')

        '''
        Si on considere que c'est un poil c'est que
        C'est une region etendue (ratio longueur largeur) de la longueur d'un cheveux
        Ou si il y a un croisement entre deux cheveux (forme X) : la solidite tombe tres bas (deux lignes 
        longues donne une box large et longue mais remplie avec juste deux cheveux)
        '''

        if (region.major_axis_length > HAIRS_LENGTH and region.minor_axis_length/region.major_axis_length < HAIRS_WIDTH_LENGTH_RATIO) or (region.major_axis_length > HAIRS_LENGTH and region.solidity < HAIRS_MAX_SOLIDITY):

            for x, y in region.coords:
                filled[x, y] = 1
                detected[x][y] = [255, 255, 255]

    axs[5].imshow(detected)
    axs[5].set_title("Detected")

    # --- Applying a erossion to make hairs thinner ---
    eroded = erosion(filled, disk(EROSION_SIZE))

    final_mask = eroded

    fig, ax = plt.subplots(1, 3, figsize=(16, 9))
    ax[0].imshow(filled, cmap='gray')
    ax[0].set_title('filled (hairs identified)')
    ax[1].imshow(original, cmap='gray')
    ax[1].set_title('Original')
    ax[2].imshow(eroded, cmap='gray')
    ax[2].set_title('Eroded (hair thinners)')

    if plotting:
        pass
        # plt.show()
        if saving:
            save_path = './ph2-processed/' + \
                IMAGE_PATH.split('/')[-1][:-3] + '.jpg'
            fig.savefig(save_path, dpi=300)
            if close_fig :
                plt.close(fig)
            
    return original, gray, final_mask

# %%


def remove_hair(IMAGE_PATH, original, mask, plotting=True, saving=False, close_fig=False, **kwargs):
    """
    Remove the hairs from the image.
    """
    options = {
        'KERNEL_EXT_SIZE': 15,  # based on hairs width
        'KERNEL_INT_SIZE': 6,
    }  # Size of the kernel used for the convolution
    KERNEL_EXT_SIZE = options['KERNEL_EXT_SIZE']
    KERNEL_INT_SIZE = options['KERNEL_INT_SIZE']
    KERNEL_EXT = disk(KERNEL_EXT_SIZE)
    # KERNEL_EXT = KERNEL_EXT/np.sum(KERNEL_EXT)

    KERNEL_INT = disk(KERNEL_INT_SIZE)
    # --- Pad the array to get a ring ---
    padder = KERNEL_EXT_SIZE-KERNEL_INT_SIZE
    KERNEL_INT_PADDED = np.pad(KERNEL_INT, [padder, padder], mode='constant')
    KERNEL = KERNEL_EXT - KERNEL_INT_PADDED

    # Applying convolution
    convolved = convolve2d(gray, KERNEL, mode='same',
                           boundary='symm')/np.sum(KERNEL)

    removed = original*(1-mask) + convolved*(mask)

    # --- Plotting ---

    # Add the kernel to convolution image to visualize its size
    for i, j in np.ndindex(KERNEL.shape):
        convolved[i, j] = KERNEL[i, j]

    if plotting:
        fig, axs = plt.subplots(2, 2, figsize=(16, 9))
        axs = np.ravel(axs)
        axs[0].imshow(original, vmin=0, vmax=1)
        axs[0].set_title("Original + KERNEK")

        axs[1].imshow(convolved, vmin=0, vmax=1)
        axs[1].set_title("Convolved")

        axs[2].imshow(removed, vmin=0, vmax=1)
        axs[2].set_title("Removed")
        axs[3].imshow((1-mask)*original, vmin=0, vmax=1)
        axs[3].set_title("mask")

        if saving:
            save_path = './ph2-processed/' + \
                IMAGE_PATH.split('/')[-1][:-3] + '.jpg'
            fig.savefig(save_path, dpi=300)
            if close_fig :
                plt.close(fig)


# %%
if __name__ == '__main__':
    # t0 = time.perf_counter()
    # identify_hairs("ph2-dataset/IMD027.bmp", plotting=True, saving=True)
    # t1 = time.perf_counter()
    # plt.show()
    # t2 = time.perf_counter()
    # identify_hairs("ph2-dataset/IMD027.bmp", plotting=True, saving=False)
    # t3 = time.perf_counter()
    # plt.show()
    # t4 = time.perf_counter()
    # identify_hairs("ph2-dataset/IMD027.bmp", plotting=False, saving=False)
    # t5 = time.perf_counter()
    # print(f'plotting saving = {t1-t0}') # = 5.772121927002445 (100dpi) or 8.391543238998565 (600dpi)
    # print(f'plotting no(saving)  = {t3-t2}') # = 5.772121927002445
    # print(f'no(plotting) no(saving) = {t5-t4}') # = 5.175234038004419

    # # Profiling to measure time of each step (with snakeviz, pip install possible)
    # import cProfile
    # import pstats

    # with cProfile.Profile() as pr:
    #     identify_hairs("ph2-dataset/IMD027.bmp", plotting=True, saving=True)
    # stats = pstats.Stats(pr)
    # stats.sort_stats(pstats.SortKey.TIME)

    # stats.dump_stats(filename='profile_stats.prof')

    import os
    from tqdm import tqdm
    files = os.listdir('./ph2-dataset/')
    for f in tqdm(files):
        original, gray, closed = identify_hairs('./ph2-dataset/'+f, plotting=True, saving=False, close_fig=True)
        remove_hair('./ph2-dataset/'+f,gray, closed, plotting=True, saving=True, close_fig=True)
